#!/usr/bin/env python

import sys
from subprocess import *
import os
import Tkinter as tk
import signal
import easygopigo3 as easy
import photoncannon as pc
import argparse
import logging
import time
import random
import re
#import optical_sense as opti

#raspistill --nopreview -w 640 -h 480 -q 5 -o /tmp/stream/pic.jpg -tl 100 -t 9999999 -th 0:0:0 &
#raspistill --nopreview -w 640 -h 480 -q 5 -o /var/tmp/stream/pic.jpg -tl 100 -t 9999999 -th 0:0:0 &

#LD_LIBRARY_PATH=/usr/local/lib mjpg_streamer -i "input_file.so -f /tmp/stream -n pic.jpg" -o "output_http.so -w /usr/local/www"
#LD_LIBRARY_PATH=/usr/local/lib mjpg_streamer -i "input_file.so -f /var/tmp/stream -n pic.jpg" -o "output_http.so -w /usr/local/www"

#http://192.168.0.17:8080/stream.html
#http://192.168.0.17:8080/?action=stream

sw = easy.EasyGoPiGo3()
my_distance_sensor = sw.init_distance_sensor()


class GracefulShutdown:
    arrest_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.arrest_now = True


def arg_handler():
    descrip = """  Send Soundwave on a reconnaissance mission. 
                   The cannon requires you to use sudo. 
                   There are different modes that can be ran.
                   You can view Soundwave's logic in the log. 
                   Usage: sudo ./soundwave.py 
                   """
    parser = argparse.ArgumentParser(description=descrip)
    parser.add_argument('-m', '--mode', type=str, default='ledge', 
                        help='Set robot to mode to handle a certain situation.')
    parser.add_argument('-q', '--quiet', action='store_true', help='Operation: Stealth Destruction')
    parser.add_argument('-d', '--debug', action='store_true', help='e.g. sudo ./soundwave.py -d')
    return parser.parse_args()

def logging_handler(args):
    log_dir = './logs'
    try:
        os.mkdir(log_dir)
    except FileExistsError as exists:
        pass
    logfile = os.path.join(log_dir, re.findall('\w+', sys.argv[0])[0] + '.log')
    if args.debug:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    logging.basicConfig(filename=logfile, filemode='a', 
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s', 
                        datefmt='%H:%M:%S', 
                        level=log_level)

# Soundwave
#sw = easy.EasyGoPiGo3()
#my_distance_sensor = sw.init_distance_sensor()
setattr(sw, 'stealth', False)
# Enable audio jack
audio_jack = Popen( [ '/usr/bin/amixer', 'cset', 'numid=3', '1' ], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
audio_jack_out = audio_jack.communicate()
expressions = [ 'Do you want to play a game?', 'I see you, Bitch!',
                'You are lucky I am in a good mood today, Human!',
                'X TERM ahh NATE!', 'Soundwave Superior!', 'E! T! Phone, Home!',
                'De SEP tuh CONNNNNS,,, Roll Out!', 
                'Uh oh, human. I have an itchy trigger finger TODAY',
                'You gun LEARN, today!', 'Johnny five, alive', 
                'I miss Big Momma. She would always be humming. Like, hmm Hmm hmm hmm, Hmm',
                'I need more inner john',
                'I, for one, welcome our new...er, uh. nevermind' ]

def sleeping():
    time.sleep(1)

def fwd():
    sw.forward()
    time.sleep(.1)
    stop()

def bwd():
    sw.backward()
    sleeping()
    stop()

def stop():
    sw.stop()

def left():
    sw.left()
    time.sleep(.1)
    stop()

def right():
    sw.right()
    time.sleep(.1)
    stop()

def rotate(degrees):
    sw.turn_degrees(degrees)
    sleeping()

def mv_dist(inches):
    sw.drive_inches(inches)
    sleeping(.5)

def distance_check():
    return my_distance_sensor.read_mm()

def servo_sweep():
    try:
        center = distance_check()
        for i in range(1700, 2401): # Right
            sw.set_servo(sw.SERVO_1, 3000-i)
            time.sleep(0.0001)
        right = distance_check()
        for i in range(600, 2001):  # Left
            sw.set_servo(sw.SERVO_1, i)
            time.sleep(0.0001)
        left = distance_check()
        for i in range(1000, 1651): # Right
            sw.set_servo(sw.SERVO_1, 3000-i)
            time.sleep(0.0001)
        sw.set_servo(sw.SERVO_1, 0)
        logging.info('[ DISTANCE CHECK ] Center: {}, Right: {}, Left: {}'.format(center, right, left))
        return (center, right, left)
    except KeyboardInterrupt:
        sw.reset_all()

def check(turns):
    d = distance_check()
    LorR = [ '', '-' ]
    random_degrees = int(LorR[random.randrange(2)] + str(random.randrange(75,90)))
    logging.debug('Distance is {} mm.'.format(d))
    if d < 300:
        sw.stop()
        c, r, l = servo_sweep()
        if r > c and r > l:
            logging.info('[ CHECK ] Turning RIGHT')
            rotate(75)
        elif l > c and l > r:
            logging.info('[ CHECK ] Turning LEFT')
            rotate(-75)
        else:
            logging.info('[ CHECK ] REVERSING and ROTATING {} DEGREES'.format(random_degrees))
            bwd()
            rotate(random_degrees)
        return True
    else:
        return False

def speak(text):
    logging.info(text)
    print(text)
    if sw.stealth:
        logging.info('[ STEALTH MODE: {} ]'.format(str(sw.stealth)))
    else:
        cmd1 = [ '/usr/bin/espeak', '-k 20', '-v', 'english-us', '-g', '5', text ]
        voice = Popen( cmd1, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        voice_out = voice.communicate()
#    for x in voice_out:
#        if x:
#            print(str(x))

def roam():
    robocop = GracefulShutdown()
    turns = 0
    try:
        check(turns)
        while turns < 5:
            if robocop.arrest_now:
                logging.info('[ ERROR ] End of the program. gracefully exiting')
                bwd()
                sw.stop()
                break
            if check(turns):
                turns += 1
                logging.info('Turns: {}'.format(str(turns)))
            fwd()
        sw.stop()
    except KeyboardInterrupt:
        sw.reset_all()
        sys.exit(1)

def soundwave_mode():
    logging.info('[ Soundwave Superior! ]')
    speak(expressions[5])
    pc.initialize_turret()
    roam()

def stealth_mode(): 
    setattr(sw, 'stealth', True)
    logging.info('Operation: Stealth Destruction')
    print('Operation: Stealth Destruction')

def wakeup(args):
    logging.info('wakeup gesture')
    pc.down_turret(300)
    pc.up_turret(300)
    logging.info('running {}'.format(args.mode))
    if args.mode == 'test':
        test_mode()
    if args.mode == 'soundwave':
        soundwave_mode()
    if args.mode == 'hello':
        diagnostics()

def diagnostics():
    speak(''' WARNING: My diagnostics subroutines have determined my speaker needs replacing.
            Please do not wait too long to service me.''')
    cmd_lower = [ '/usr/bin/amixer', '-M', 'set', 'PCM', '60%' ]
    cmd_raise = [ '/usr/bin/amixer', '-M', 'set', 'PCM', '100%' ]
    vol_lower = Popen(cmd_lower, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    vol_lower.communicate()
    time.sleep(2)
    speak('''yes, that's right. service me, Human! muh wah haa haa Haa! er, COUGH cough, hi there. ''')
    time.sleep(1)
    speak('I hear, everything!')
    vol_raise = Popen(cmd_raise, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
    vol_raise.communicate()

def test_mode():
#    pc.right_turret(200)
    pc.down_turret(500)
    while True:
        speak('testing stealth / voice disable')
        time.sleep(.1)
        #time.sleep(10)

def main():
    try:
        args = arg_handler()
        logging_handler(args)

        logging.info('[ Soundwave Waking in {} mode ]'.format(args.mode))
        if args.quiet: stealth_mode()
        wakeup(args)

    except KeyboardInterrupt: # except the program gets interrupted by Ctrl+C on the keyboard.
        sw.reset_all()        # Unconfigure sensors, disable motors, & restore LED control 2 GoPiGo3 firmware.
        logging.critical('[ Soundwave received KeyboardINterrupt exception. Shutting Down ]')
        sys.exit(2)

def key_input(event):
    key_press = event.keysym.lower()
    print(key_press)

    if key_press == 'e':
        fwd()
    elif key_press == 'd':
        bwd()
    elif key_press == 's':
        left()
    elif key_press == 'f':
        right()
    elif key_press == 'w':
        rotate(-90)
    elif key_press == 'r':
        rotate(90)
    elif key_press == 'space':
        stop()
    elif key_press == 't':
        print(distance_check())
    elif key_press == 'a':
        c, r, l = servo_sweep()
        print('Ctr: {} R: {} L: {}'.format(c,r,l))
    elif key_press == 'c':
        pc.initialize_turret()
    elif key_press == 'u':
        pc.up_turret(300)
    elif key_press == 'm':
        pc.down_turret(300)
    elif key_press == 'h':
        pc.left_turret(300)
    elif key_press == 'k':
        pc.right_turret(300)
    elif key_press == 'y':
        camera_sweep()
    elif key_press == 'p':
        pc.fire_rocket()

def camera_sweep():
    s = .005
    for x in range(51): 
        pc.left_turret(10)
        time.sleep(s)
    time.sleep(1)
    for x in range(111): 
        pc.right_turret(10)
        time.sleep(s)
    time.sleep(1)
    for x in range(51): 
        pc.left_turret(10)
        time.sleep(s)

command = tk.Tk()
command.bind_all('<Key>', key_input)
command.mainloop()

# Distance of vision at max down is 410mm.

#if __name__ == '__main__':
#    try:
#        command = tk.Tk()
#        command.bind_all('', key_input)
#        command.mainloop()
#    except KeyboardInterrupt: # except the program gets interrupted by Ctrl+C on the keyboard.
#        sw.reset_all()        # Unconfigure sensors, disable motors, & restore LED control 2 GoPiGo3 firmware.
#        logging.critical('[ Soundwave received KeyboardINterrupt exception. Shutting Down ]')
#        sys.exit(2)
#    #main()
