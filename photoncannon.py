#!/usr/bin/env python3

import signal
import logging
import time
import sys
import re
import os
###
import platform
import usb.core
import usb.util

tdelay=80

# Protocol command bytes
DOWN    = 0x01
UP      = 0x02
LEFT    = 0x04
RIGHT   = 0x08
FIRE    = 0x10
STOP    = 0x20

DEVICE = None
DEVICE_TYPE = None

# Setup the Office Cannon
def setup_usb():
    global DEVICE 
    global DEVICE_TYPE

    DEVICE = usb.core.find(idVendor=0x2123, idProduct=0x1010)

    if DEVICE is None:
        DEVICE = usb.core.find(idVendor=0x0a81, idProduct=0x0701)
        if DEVICE is None:
            raise ValueError('Missile device not found')
        else:
            DEVICE_TYPE = "Original"
    else:
        DEVICE_TYPE = "Thunder"

    # On Linux we need to detach usb HID first
    if "Linux" == platform.system():
        try:
            DEVICE.detach_kernel_driver(0)
        except Exception as e:
            pass # already unregistered    
    DEVICE.set_configuration()

#Send command to the office cannon
def send_cmd(cmd):
    if "Thunder" == DEVICE_TYPE:
        DEVICE.ctrl_transfer(0x21, 0x09, 0, 0, [0x02, cmd, 0x00,0x00,0x00,0x00,0x00,0x00])
    elif "Original" == DEVICE_TYPE:
        DEVICE.ctrl_transfer(0x21, 0x09, 0x0200, 0, [cmd])

#Send command to control the LED on the office cannon
def led(cmd):
    if "Thunder" == DEVICE_TYPE:
        DEVICE.ctrl_transfer(0x21, 0x09, 0, 0, [0x03, cmd, 0x00,0x00,0x00,0x00,0x00,0x00])
    elif "Original" == DEVICE_TYPE:
        print("There is no LED on this device")

#Send command to move the office cannon
def send_move(cmd, duration_ms):
    send_cmd(cmd)
    time.sleep(duration_ms / 1000.0)
    send_cmd(STOP)

def run_command(command, value):
    command = command.lower()
    if command == "right":
        send_move(RIGHT, value)
    elif command == "left":
        send_move(LEFT, value)
    elif command == "up":
        send_move(UP, value)
    elif command == "down":
        send_move(DOWN, value)
    elif command == 'initialize' or command == "zero" or command == "park" or command == "reset":
        # Move to bottom-left; then center , up x2
        logging.info('Arming Photon Torpedoes')
        send_move(DOWN, 2000)
        send_move(LEFT, 8000)
        send_move(RIGHT, 3000)
        send_move(UP, tdelay*2)
    elif command == "pause" or command == "sleep":
        time.sleep(value / 1000.0)
    elif command == "led":
        if value == 0:
            led(0x00)
        else:
            led(0x01)
    elif command == "fire" or command == "shoot":
        if value < 1 or value > 4:
            value = 1
        # Stabilize prior to the shot, then allow for reload time after.
        time.sleep(0.5)
        for i in range(value):
            send_cmd(FIRE)
            time.sleep(4.5)
    else:
        print("Error: Unknown command: '%s'" % command)

def run_command_set(commands):
    for cmd, value in commands:
        run_command(cmd, value)

###
def logging_handler():
        log_dir = './logs'
        try:
            os.mkdir(log_dir)
        except FileExistsError as exists:
            pass
        logfile = os.path.join(log_dir, re.findall('\w+', sys.argv[0])[0] + '.log')
        logging.basicConfig(filename=logfile, filemode='a', 
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s', 
                            datefmt='%H:%M:%S', 
                            level=logging.DEBUG) 

class GracefulShutdown:
    arrest_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.arrest_now = True

def initialize_turret():
    run_command("initialize", 100)

def fire_rocket():
    logging.info('[ Firing Photon Torpedo! ]')
    run_command("fire", tdelay)

def left_turret(td=tdelay):
    logging.info(' - Turret LEFT')
    run_command("left", td)

def right_turret(td=tdelay):
    logging.info(' - Turret RIGHT')
    run_command("right", td)

def up_turret(td=tdelay):
    logging.info(' - Turret UP')
    run_command("up", td)

def down_turret(td=tdelay):
    logging.info(' - Turret DOWN')
    run_command("down", td)

def photon_torpedoes():
    ''' Arming the Photon Torpedoes
        enable_sound: enterprise moving
        sound: enterprise weapon
        logging: Arming the Photon Torpedoes!
        logic: hunt mode
    '''
    try:
        # Basic movements for now
        #initialize_turret()
        right_turret(200)
        up_turret(100)
        left_turret(100)
#        down_turret(200)
##        fire_rocket()
##        fire_rocket()
#        initialize_turret()
    except Exception as e:
        logging.warning(str(e))
        logging.critical('Exception hit. Exiting')
        sys.exit(2)

def main():
    try:
        photon_torpedoes()
    except Exception as e:
        logging.warning(str(e))
        logging.critical('Exception hit. Exiting')

try:
    setup_usb()
except Exception as e:
    logging.warning(str(e))
    logging.critical('Exception hit on setup_usb. Exiting')

if __name__ == '__main__':
    logging_handler()
    main()
