#!/usr/bin/env python3

import io
import picamera
import numpy as np
import cv2
from PIL import Image
import datetime
import logging
import os
from time import sleep

def recognize_faces(img_dir='./results/',replace=False):
    try:
        os.mkdir(img_dir)
    except Exception as e:
        pass

    #Create a memory stream so photos doesn't need to be saved in a file
    stream = io.BytesIO()
    
    #Get the picture (low resolution, so it should be quite fast)
    #Here you can also specify other parameters (e.g.:rotate the image)
    with picamera.PiCamera() as camera:
        #camera.resolution = (1600, 1200)
        camera.resolution = (800, 600)
        #camera.resolution = (320, 240)
        camera.capture(stream, format='jpeg')
    
    #Convert the picture into a numpy array
    buff = np.fromstring(stream.getvalue(), dtype=np.uint8)
    
    #Now creates an OpenCV image
    image = cv2.imdecode(buff, 1)
    
    #Load cascade files for detecting humans
    face_cascade = cv2.CascadeClassifier('/home/pi/gobot_shared/OpenCV/haarcascade_frontalface_default.xml')
    profile_cascade = cv2.CascadeClassifier('/home/pi/gobot_shared/OpenCV/haarcascade_profileface.xml')
    body_cascade = cv2.CascadeClassifier('/home/pi/gobot_shared/OpenCV/haarcascade_fullbody.xml')
    
    #Convert to grayscale
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    
    #Look for faces in the image using the loaded cascade file
    faces = face_cascade.detectMultiScale(gray, 1.1, 5)
    profiles = profile_cascade.detectMultiScale(gray, 1.1, 5)
    bodies = body_cascade.detectMultiScale(gray, 1.1, 5)

    haars = [ faces, profiles, bodies ]
    haar_len = 0
    for haar in haars:
        haar_len += len(haar)
    
    print('Found '+str(len(faces))+' face(s)')
    print('Found '+str(len(profiles))+' proflie(s)')
    print('Found '+str(len(bodies))+' body(s/y/ies/)')
    
    #Draw a rectangle around every found part
    for haar in haars:
        for (x,y,w,h) in haar:
            cv2.rectangle(image,(x,y),(x+w,y+h),(255,255,0),2)
    
    #Save the result image
    if replace:
        if haar_len > 0:
            image_name = os.path.join(img_dir,'result.jpg')
            cv2.imwrite(image_name,image)
            overlayer(image_name)
    else:
        if haar_len > 0:
            now = datetime.datetime.now()
            image_name = os.path.join(img_dir,
                        'result_{n.month}-{n.day}-{n.year}_{n.hour}-{n.minute}-{n.second}.jpg'.format(n=now))
            logging.info('Taking snapshot {}'.format(image_name))
            cv2.imwrite(image_name,image)
            overlayer(image_name)
    return haar_len

def overlayer(image_name):
    overlay_d = { 'Decepticons.thumbnail':0, 'Soundwave.thumbnail':0}
    for foreground_img in overlay_d.keys():
        background = Image.open(image_name)
        bw, bh = background.size
        foreground = Image.open(foreground_img)
        fw, fh = foreground.size
        overlay_d = {'Decepticons.thumbnail':(bw-fw,bh-fh), 'Soundwave.thumbnail':(0,bh-fh)}
        background.paste(foreground, overlay_d[foreground_img], foreground)
        background.save(image_name)

def video_20_sec(img_dir='./results/'):
    try:
        os.mkdir(img_dir)
    except Exception as e:
        pass
    stream = io.BytesIO()
    now = datetime.datetime.now()
    image_name = os.path.join(img_dir,
                 'video20sec_{n.month}-{n.day}-{n.year}_{n.hour}-{n.minute}-{n.second}.h264'.format(n=now))
    with picamera.PiCamera() as camera:
        camera.resolution = (1600, 1200)
        camera.start_recording(image_name)
        camera.annotate_text = '20 seconds of your life!'
        camera.wait_recording(20)
        #sleep(20) camera.wait_recording handles vid recording errors.
        camera.stop_recording()

if __name__ == '__main__':
    logging.info('Camera Module')
    print('Camera Module')
    #video_20_sec()
